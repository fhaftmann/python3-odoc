#!/usr/bin/python3 -I

# Author: 2023 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# This package is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; version 2.
#
# This package is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>
#
# On Debian systems, the complete text of the GNU General
# Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


from collections.abc import Sequence
from dataclasses import dataclass
from pathlib import Path
import os
import subprocess
import argparse
import argcomplete

import toml


prefix = Path('debian')


@dataclass(frozen = True)
class Project:
    pypackage: str
    version: str
    pycmds: Sequence[Path]


def read_pyproject() -> Project:

    raw = toml.load('pyproject.toml')
    name = raw['project']['name']
    version = raw['project']['version']
    pypackage = f'python3-{name}'
    bin_prefix = prefix / pypackage / 'usr' / 'bin'
    return Project(pypackage = pypackage, version = version,
      pycmds = [bin_prefix / s for s in raw['project']['scripts'].keys()])


def generate_man_pages(project: Project) -> None:

    for pycmd in project.pycmds:
        subprocess.run(['docmaker', 'help2man',
          '--output-dir', 'man',
          pycmd.name, '1', project.version, 'python3', pycmd],
          env = os.environ | dict(PYTHONPATH = '.'),
          check = True)


def generate_bash_completions(project: Project) -> None:

    completion_summary = prefix / f'{project.pypackage}.bash-completion'
    completion_prefix = prefix / f'{project.pypackage}.bash-completions'

    completions = []
    completion_prefix.mkdir(exist_ok = True)
    for pycmd in project.pycmds:
        name = pycmd.name
        result = subprocess.run(['register-python-argcomplete', name],
          stdout = subprocess.PIPE, text = True, check = True)
        completion = completion_prefix / name
        completion.write_text(result.stdout)
        completions.append(f'{completion}\n')

    completion_summary.write_text(''.join(completions))


parser = argparse.ArgumentParser()
argcomplete.autocomplete(parser)
args = parser.parse_args()

os.chdir(Path(__file__).parent)

project = read_pyproject()

generate_bash_completions(project)
generate_man_pages(project)
