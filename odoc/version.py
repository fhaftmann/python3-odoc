
'''Handling version numbers using existing open document infrastructure
as far as appropriate.'''

# Author: 2023 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# This package is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; version 2.
#
# This package is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>
#
# On Debian systems, the complete text of the GNU General
# Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


from typing import Optional, cast
from os import path

from lxml import etree  # type: ignore

from . import util
from . import extension
from . import mime
from . import manifest
from . import raw
from .normalization import namespace_office


namespace_meta = 'urn:oasis:names:tc:opendocument:xmlns:meta:1.0'

name_database_version = 'version.txt'


def for_document(loc: str) -> Optional[str]:

    loc_meta = path.join(loc, raw.name_meta)
    doc = util.read_doc_permissive(loc_meta)
    if doc is None:
        util.info('WARNING – bad xml: {}'.format(loc_meta))
        return None

    for elem in doc.getroot().findall('*/{' + namespace_meta + '}user-defined'):
        if elem.get(util.format_qname((namespace_meta, 'name'))) == 'Version':
            return cast(str, elem.text)

    return None


def for_database(loc: str) -> Optional[str]:

    loc_version = path.join(loc, name_database_version)
    if path.isfile(loc_version):
        with open(loc_version, encoding = util.utf8) as reader:
            return reader.read().rstrip()

    return None


def for_extension(loc: str) -> Optional[str]:

    loc_description = path.join(loc, extension.name_description)
    doc = util.read_doc_permissive(loc_description)
    if doc is None:
        util.info('WARNING – bad xml: {}'.format(loc_description))
        return None

    return cast(str, doc.getroot().find('{' + extension.namespace_description + '}' + 'version').get('value'))


def for_directory(loc: str) -> Optional[str]:

    mime_type = mime.guess_type_of_directory(loc)
    return for_extension(loc) if mime_type == mime.type.extension \
      else for_database(loc) if mime_type == mime.type.database \
      else for_document(loc)


def set_for_document(loc: str, version: str) -> None:

    loc_meta = path.join(loc, raw.name_meta)

    doc = etree.parse(loc_meta)
    elem = doc.find(util.format_qname((namespace_office, 'meta')))
    elem_version = etree.Element(util.format_qname((namespace_meta, 'user-defined')),
      {util.format_qname((namespace_meta, 'name')): 'Version'})
    elem_version.text = version
    elem.append(elem_version)

    util.write_doc(loc_meta, doc)


def set_for_database(loc: str, version: str) -> None:

    with open(path.join(loc, name_database_version), 'w', encoding = util.utf8) as writer:
        writer.write(version + '\n')

    manifest.ensure_entry(loc, name_database_version, 'text/plain')


def set_for_extension(loc: str, version: str) -> None:

    loc_description = path.join(loc, extension.name_description)
    description = extension.force_version_for_description(loc_description, version)

    with open(loc_description, 'wb') as writer:
        writer.write(description)


def set_for_directory(loc: str, version: str) -> None:

    mime_type = mime.guess_type_of_directory(loc)
    if mime_type == mime.type.extension:
        set_for_extension(loc, version)
    elif mime_type == mime.type.database:
        set_for_database(loc, version)
    else:
        set_for_document(loc, version)
