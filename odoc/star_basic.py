
'''Treatment for Star Basic.'''

# Author: 2023 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# This package is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; version 2.
#
# This package is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>
#
# On Debian systems, the complete text of the GNU General
# Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


__all__ = ['planify', 'xmlify']


from collections.abc import Iterator, Sequence
import os
from os import path
from xml.sax.saxutils import XMLGenerator

from lxml import etree  # type: ignore

from . import util


doctype = '<!DOCTYPE script:module PUBLIC "-//OpenOffice.org//DTD OfficeDocument 1.0//EN" "module.dtd">'
prefix = 'script'
namespace = 'http://openoffice.org/2000/script'
tag_module = namespace, 'module'
attr_name = namespace, 'name'
attr_language = namespace, 'language'
attr_language_value = 'StarBasic'
ext_plain = 'star_basic'
ext_xba = 'xba'
ext_xml = 'xml'


def list_document(loc_dir: str) -> Iterator[str]:

    for name in util.list_files_tree(path.join(loc_dir, 'Basic')):
        if path.splitext(name)[1] == '.' + ext_xml:
            yield path.join('Basic', name)


def list_extension(loc_dir: str) -> Iterator[str]:

    for name in util.list_files_tree(loc_dir):
        if path.splitext(name)[1] == '.' + ext_xba:
            yield name


def planify(loc_dir: str, is_extension: bool, delete: bool) -> None:  ## FIXME self-contained interface

    list_files = list_extension if is_extension else list_document

    for name in list_files(loc_dir):

        src = path.join(loc_dir, name)
        with open(src, 'rb') as reader:
            content = reader.read()

        if content == b'':
            util.info('WARNING – empty star basic file: {}'.format(src))
            os.remove(src)
            continue

        try:
            root = etree.XML(content)
        except etree.XMLSyntaxError:
            util.info('WARNING – bad xml: {}'.format(src))
            continue
        module_name = root.attrib.get(util.format_qname(attr_name))
        if module_name is None:
            continue
        bare_name = path.splitext(path.basename(name))[0]
        if bare_name != module_name:
            raise Exception(f'Module name mismatch in file »{name}«: star basic module named »{module_name}« must'
              'reside in a file with same bare name')
        txt = str(root.text)
        with open(path.join(loc_dir, path.splitext(name)[0]) + '.' + ext_plain,
          'w', encoding = util.utf8) as writer:
            writer.write(txt)
        if delete:
            os.remove(src)


def xmlify(loc_dir: str, is_extension: bool, delete: bool) \
  -> tuple[Sequence[str], Sequence[str]]:  ## FIXME self-contained interface

    ext = ext_xba if is_extension else ext_xml

    processed = []
    generated = []

    for loc_rel in util.list_files_tree(loc_dir):

        if path.splitext(loc_rel)[1] != '.' + ext_plain:
            continue

        src = path.join(loc_dir, loc_rel)
        dst = path.splitext(src)[0] + '.' + ext
        with open(src, encoding = util.utf8) as reader:
            content = reader.read()
        with open(dst, 'w') as writer:
            generator = XMLGenerator(writer)  ## FIXME prefer etree
            generator.startDocument()
            writer.write(doctype + '\n')
            generator.startPrefixMapping(prefix, namespace)
            generator.startElementNS(tag_module, tag_module[1],
              {attr_name: path.splitext(path.basename(loc_rel))[0],  ## FIXME proper filename encoding
                attr_language: attr_language_value})  # type: ignore
            generator.characters(content)
            generator.endElementNS(tag_module, tag_module[1])
            generator.endPrefixMapping(prefix)
            generator.endDocument()
        if delete:
            os.remove(src)
        processed.append(loc_rel)
        generated.append(dst)

    return processed, generated
