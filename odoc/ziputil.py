
'''Convenience operations on ZIP files.'''

# Author: 2023 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# This package is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; version 2.
#
# This package is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>
#
# On Debian systems, the complete text of the GNU General
# Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


__all__ = ['extract', 'create']


from collections.abc import Iterable, Iterator
from contextlib import contextmanager
from os import path
import os
from zipfile import ZipFile, ZIP_DEFLATED, ZIP_STORED, BadZipfile


def check_path(loc: str) -> None:

    if path.isabs(loc):
        raise BadZipfile('Absolute path in zip archive: {}'.format(loc))
    if path.normpath(loc).startswith(os.pardir):
        raise BadZipfile('Parent path in zip archive: {}'.format(loc))


@contextmanager
def read(loc: str) -> Iterator[ZipFile]:

    with ZipFile(loc) as zip_file:
        for entry in zip_file.infolist():
            check_path(entry.filename)
        yield zip_file


def get_file(loc: str, loc_rel: str) -> bytes:

    with read(loc) as zip_file:
        content: bytes = zip_file.read(loc_rel)
    return content


def extract(src: str, dst: str) -> None:

    with read(src) as zip_file:
        zip_file.extractall(dst)


def create(dst: str, srcs: Iterable[tuple[str, bytes, bool]]) -> None:

    with ZipFile(dst, 'w', compression = ZIP_DEFLATED) as zip_file:
        for loc_rel, content, compressed in srcs:
            zip_file.writestr(loc_rel, content,
              ZIP_DEFLATED if compressed else ZIP_STORED)
