
'''File system iterators concerning open documents.'''

# Author: 2023 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# This package is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; version 2.
#
# This package is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>
#
# On Debian systems, the complete text of the GNU General
# Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


__all__ = ['open_document_files', 'open_document_directories']


from collections.abc import Iterator
from os import path
import os

from . import mime


def open_document_files(loc: str) -> Iterator[tuple[str, str]]:

    if not path.isdir(loc):
        some_mime_type = mime.guess_type(loc)
        if some_mime_type is not None:
            yield loc, some_mime_type

    for dir_loc, _, file_names in os.walk(loc):
        for file_name in file_names:
            loc = path.join(dir_loc, file_name)
            some_mime_type = mime.guess_type(loc)
            if some_mime_type is not None:
                yield loc, some_mime_type


def open_document_directories(loc: str) -> Iterator[tuple[str, str]]:

    if not path.isdir(loc):
        raise Exception('Bad directory: {}'.format(loc))

    for dir_loc, _, _ in os.walk(loc):
        some_mime_type = mime.guess_type_of_directory(dir_loc)
        if some_mime_type is not None:
            yield dir_loc, some_mime_type
