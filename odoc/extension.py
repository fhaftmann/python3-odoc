
"""Properties of extensions."""

# Author: 2023 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# This package is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; version 2.
#
# This package is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>
#
# On Debian systems, the complete text of the GNU General
# Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


__all__ = ['name_description', 'force_version_for_description']


from typing import cast

from lxml import etree  # type: ignore

from . import util


name_description = 'description.xml'
namespace_description = 'http://openoffice.org/extensions/description/2006'


def force_version_for_description(loc: str, version: str) -> bytes:

    doc = etree.parse(loc)

    root = doc.getroot()
    elem = root.find('{' + namespace_description + '}' + 'version')
    if elem is None:
        root.insert(1, etree.Element('version', dict(value = version)))
        util.indent_elem(root, set(), 4)
    else:
        elem.set('value', version)

    return cast(bytes, etree.tostring(doc, encoding = doc.docinfo.encoding,
      standalone = doc.docinfo.standalone, xml_declaration = True))
