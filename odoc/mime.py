
'''Open Document MIME types and associated file extensions.'''

# Author: 2023 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# This package is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; version 2.
#
# This package is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>
#
# On Debian systems, the complete text of the GNU General
# Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


from typing import Optional

from os import path
import mimetypes

from . import util
from . import extension


# mime types

location = 'mimetype'

class type:

    text = 'application/vnd.oasis.opendocument.text'
    text_template = 'application/vnd.oasis.opendocument.text-template'
    text_master = 'application/vnd.oasis.opendocument.text-master'
    spreadsheet = 'application/vnd.oasis.opendocument.spreadsheet'
    spreadsheet_template = 'application/vnd.oasis.opendocument.spreadsheet-template'
    presentation = 'application/vnd.oasis.opendocument.presentation'
    presentation_template = 'application/vnd.oasis.opendocument.presentation-template'
    graphics = 'application/vnd.oasis.opendocument.graphics'
    graphics_template = 'application/vnd.oasis.opendocument.graphics-template'
    chart = 'application/vnd.oasis.opendocument.chart'
    chart_template = 'application/vnd.oasis.opendocument.chart-template'
    formula = 'application/vnd.oasis.opendocument.formula'
    formula_template = 'application/vnd.oasis.opendocument.formula-template'
    image = 'application/vnd.oasis.opendocument.image'
    image_template = 'application/vnd.oasis.opendocument.image-template'
    database = 'application/vnd.oasis.opendocument.base'
    extension = 'application/vnd.openofficeorg.extension'

    aliasses = {
        'application/vnd.sun.xml.base': database,
        'application/vnd.oasis.opendocument.database': database
    }

    documents = [text, text_template, text_master, spreadsheet, spreadsheet_template,
      presentation, presentation_template, graphics, graphics_template,
      chart, chart_template, formula, formula_template, image, image_template]


# extensions

extension_for = {
  type.text: 'odt',
  type.text_template: 'ott',
  type.text_master: 'odm',
  type.spreadsheet: 'ods',
  type.spreadsheet_template: 'ots',
  type.presentation: 'odp',
  type.presentation_template: 'otp',
  type.graphics: 'odg',
  type.graphics_template: 'otg',
  type.chart: 'odc',
  type.chart_template: 'otc',
  type.formula: 'odf',
  type.formula_template: 'otf',
  type.image: 'odi',
  type.image_template: 'oti',
  type.database: 'odb',
  type.extension: 'oxt'
}

for_extension = dict((extension_for[mime], mime)
  for mime in extension_for)

def assert_odoc(mime_type: Optional[str]) -> str:

    if mime_type is None:
        raise Exception('Unable to identify MIME type')
    if mime_type not in extension_for:
        raise Exception('Bad MIME type: {}'.format(mime_type))
    return mime_type

def is_database(mime_type: str) -> bool:
    return mime_type == type.database

def is_extension(mime_type: str) -> bool:
    return mime_type == type.extension


# guessing

def guess_type(loc: str) -> Optional[str]:

    candidate1, _ = mimetypes.guess_type(loc)
    candidate2 = type.aliasses.get(candidate1, candidate1) \
      if candidate1 is not None else None
    return candidate2 \
      if candidate2 is not None \
      else for_extension.get(path.splitext(loc)[1][1:])

def guess_type_of_directory(loc: str) -> Optional[str]:

    loc_mime_type = path.join(loc, location)
    if not path.exists(loc_mime_type):
        loc_description = path.join(loc, extension.name_description)
        if path.exists(loc_description):
            with open(loc_description, encoding = util.utf8) as reader:  ## FIXME proper XML reading
                for line in reader:
                    if line.find('http://openoffice.org/extensions') >= 0:
                        return type.extension
            return None
        else:
            return None
    with open(loc_mime_type, encoding = util.utf8) as reader:
        raw_mime_type = reader.read().rstrip()
        mime_type = type.aliasses.get(raw_mime_type, raw_mime_type)
    if mime_type not in extension_for:
        return None
    return mime_type
