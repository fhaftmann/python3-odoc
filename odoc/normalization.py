
'''Pretty formatting of open document xml files.'''

# Author: 2023 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# This package is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; version 2.
#
# This package is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>
#
# On Debian systems, the complete text of the GNU General
# Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


__all__ = ['indent', 'hashify_styles']


from os import path

from lxml import etree  # type: ignore

from . import util
from . import raw


# mixed content tags, according to http://docs.oasis-open.org/office/v1.2/OpenDocument-v1.2.html

namespace_office = 'urn:oasis:names:tc:opendocument:xmlns:office:1.0'
namespace_text = 'urn:oasis:names:tc:opendocument:xmlns:text:1.0'
namespace_style = 'urn:oasis:names:tc:opendocument:xmlns:style:1.0'

mixed_content_office = [(namespace_office, 'script')]
mixed_content_text = [(namespace_text, 'h'), (namespace_text, 'p'),
  (namespace_text, 'span'), (namespace_text, 'a'), (namespace_text, 'meta'),
  (namespace_text, 'ruby-base'), (namespace_text, 'meta-field')]

files = {raw.name_content: mixed_content_office + mixed_content_text,
  raw.name_styles: mixed_content_office + mixed_content_text,
  raw.name_meta: mixed_content_office,
  raw.name_settings: mixed_content_office}


def indent(loc_dir: str) -> None:

    for name in files:
        loc = path.join(loc_dir, name)
        if path.exists(loc):
            util.indent_file(loc, files[name])


def hashify_styles(loc_dir: str) -> None:

    loc_styles = path.join(loc_dir, raw.name_styles)
    loc_content = path.join(loc_dir, raw.name_content)
    namespaces = dict(office = namespace_office, style = namespace_style)
    attr_name = util.format_qname((namespace_style, 'name'))

    doc_styles = etree.parse(loc_styles)
    used_names = set(str(value) for value in
      doc_styles.xpath('//office:document-styles/office:styles/*/attribute::style:name',
        namespaces = namespaces))
    name_mapping = dict()
    for automatic_style in doc_styles.xpath('//office:document-styles/office:automatic-styles/*[@style:name]',
      namespaces = namespaces):
        current_name = automatic_style.get(attr_name)
        automatic_style.set(attr_name, '')
        value = 'X' + str(abs(hash(etree.tostring(automatic_style))))
        while value in used_names:
            value += 'X'
        used_names.add(value)
        if value != current_name:
            name_mapping[current_name] = value
            automatic_style.set(attr_name, value)

    doc_content = etree.parse(loc_content)

    util.write_doc(loc_styles, doc_styles)
    util.write_doc(loc_content, doc_content)
