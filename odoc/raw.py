
'''Raw extracting and bundling of open document files.'''

# Author: 2023 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# This package is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; version 2.
#
# This package is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>
#
# On Debian systems, the complete text of the GNU General
# Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


__all__ = ['bundle', 'extract', 'name_content', 'name_styles', 'name_meta',
  'name_settings']


from typing import Optional
from collections.abc import Sequence, Iterator, Iterable
from contextlib import contextmanager
from os import path
import os
import shutil

from . import util
from . import ziputil
from . import mime
from . import manifest
from . import extension


name_content = 'content.xml'
name_styles = 'styles.xml'
name_meta = 'meta.xml'
name_settings = 'settings.xml'


def extract(src: str, dst: str, keep_destination: bool = False, delete: bool = False) -> None:

    util.info('Extracting {}…'.format(src))

    if not keep_destination and path.isdir(dst):
        shutil.rmtree(dst)
        os.mkdir(dst)
    ziputil.extract(src, dst)
    if delete:
        os.remove(src)


@contextmanager
def epheremal_dirs(locs: Sequence[str]) -> Iterator[None]:

    pending_dirs: list[str] = []
    try:
        for loc_dir in sorted(locs, key = len):
            os.mkdir(loc_dir)
            pending_dirs = [loc_dir] + pending_dirs
        yield
    finally:
        for loc_dir in pending_dirs:
            try:
                os.rmdir(loc_dir)
            except Exception:
                pass


def bundle(src: str, dst: str, delete: bool = False,
  ignored_files: Iterable[str] = set(), alternative_content: Optional[bytes] = None,
  alternative_description: Optional[bytes] = None) -> None:

    util.info('Bundling {}…'.format(src))

    if path.isfile(dst):
        os.unlink(dst)

    matching = manifest.match(src)

    if not util.null(matching.dangling_files):
        raise Exception('Files in manifest missing in file system: ' + ', '.join(list(matching.dangling_files)))

    additional_files = [loc_rel for loc_rel in matching.additional_files if loc_rel not in ignored_files]
    some_mime_type = mime.guess_type_of_directory(src)
    if some_mime_type is not None and mime.is_extension(some_mime_type):
        zip_content = matching.matching_files | set(additional_files)
    else:
        if not util.null(additional_files):
            raise Exception('Files in file system not referenced from manifest: ' + ', '.join(additional_files))
        zip_content = matching.matching_files

    with epheremal_dirs([path.join(src, loc_rel) for loc_rel in matching.dangling_dirs]):

        def srcs() -> Iterator[tuple[str, bytes, bool]]:
            loc_mime = path.join(src, mime.location)
            if path.exists(loc_mime):
                with open(loc_mime, 'rb') as reader:
                    content = reader.read()
                yield mime.location, content, False
            for loc_rel in sorted(zip_content):
                if loc_rel == mime.location:
                    continue
                if alternative_content and loc_rel == name_content:
                    yield loc_rel, alternative_content, True
                if alternative_description and loc_rel == extension.name_description:
                    yield loc_rel, alternative_description, True
                else:
                    with open(path.join(src, loc_rel), 'rb') as reader:
                        content = reader.read()
                    yield loc_rel, content, True
        ziputil.create(dst, srcs())

    if delete:
        shutil.rmtree(src)
