
'''Manifests of open document xml files.'''

# Author: 2023 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# This package is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; version 2.
#
# This package is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>
#
# On Debian systems, the complete text of the GNU General
# Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


__all__ = ['Matching', 'match', 'ensure_entry']


from typing import Optional
from collections.abc import Iterator, Sequence
from dataclasses import dataclass
from os import path

from lxml import etree  # type: ignore

from . import util
from . import mime


location = path.join('META-INF', 'manifest.xml')
namespace = 'urn:oasis:names:tc:opendocument:xmlns:manifest:1.0'

entry = namespace, 'file-entry'
entry_loc = namespace, 'full-path'
entry_type = namespace, 'media-type'


def read(loc: str) -> Iterator[tuple[Sequence[str], bool, Optional[str]]]:

    loc_manifest = path.join(loc, location)
    doc = etree.parse(loc_manifest)
    for elem in doc.getroot().findall(util.format_qname(entry)):
        loc = elem.attrib[util.format_qname(entry_loc)]
        mime_type = elem.attrib.get(util.format_qname(entry_type), None)
        if loc == '/':
            yield (), True, mime_type
        else:
            yield util.explode_path(loc), loc[-1] == '/', mime_type

    yield util.explode_path(location), False, None


def ensure_entry(loc_base: str, loc_relative: str, default_mime_type: str) -> None:

    loc_manifest = path.join(loc_base, location)
    doc = etree.parse(loc_manifest)
    root = doc.getroot()
    if not any(elem.get(util.format_qname(entry_loc)) == loc_relative for elem in root):
        elem = etree.Element(util.format_qname(entry),
          {util.format_qname(entry_loc): loc_relative, util.format_qname(entry_type): default_mime_type})
        root.append(elem)
    util.write_doc(loc_manifest, doc)


@dataclass(frozen = True)
class Matching:
    matching_files: frozenset[str]
    dangling_files: frozenset[str]
    dangling_dirs: frozenset[str]
    additional_files: frozenset[str]


def match(loc: str) -> Matching:

    content_filesys = set(util.explode_path(xs) for xs in util.list_files_tree(loc))
    content_matched = set()

    content_manifest = list(read(loc))
    content_manifest_dirs = [xs for xs, is_dir, _ in content_manifest if is_dir]
    content_manifest_files = [xs for xs, is_dir, _ in content_manifest if not is_dir]

    dangling_files = set()
    dangling_dirs = set()

    xs = util.explode_path(mime.location)
    if xs in content_filesys:
        content_manifest_files.append(xs)

    for xs in content_manifest_files:
        if xs in content_filesys:
            content_filesys.remove(xs)
            content_matched.add(xs)
        else:
            dangling_files.add(xs)

    for xs in content_manifest_dirs:
        if util.null(ys for ys in content_matched if util.is_prefix_of(xs, ys)) and \
          not path.isdir(path.join(loc, util.implode_path(xs))):
            dangling_dirs.add(xs)

    return Matching(matching_files = frozenset(util.implode_path(xs) for xs in content_matched),
      dangling_files = frozenset(util.implode_path(xs) for xs in dangling_files),
      dangling_dirs = frozenset(util.implode_path(xs) for xs in dangling_dirs),
      additional_files = frozenset(util.implode_path(xs) for xs in content_filesys)
    )
