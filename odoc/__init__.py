
'''Abstraction and manipulation of OASIS Open Document files.'''

# Author: 2023 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# This package is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; version 2.
#
# This package is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>
#
# On Debian systems, the complete text of the GNU General
# Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


long_description = '''
    This library provides abstraction and manipulation devices for OASIS Open
    Document files, plus the unofficial cousins Open Document Base files and
    LibreOffice / OpenOffice extensions.

    Particular focus is set on (un)bundling zipped files to (from) bare files
    suitable for versioning, with normalized xml, plain text basic macro text,
    implicit version numbering.
'''
