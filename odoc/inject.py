
'''Injection of content snippets using matching name tags.'''

# Author: 2023 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# This package is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; version 2.
#
# This package is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>
#
# On Debian systems, the complete text of the GNU General
# Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


__all__ = ['inject']


from typing import Any, TypeAlias
from collections.abc import Iterable, Iterator
from os import path
import shutil

from lxml import etree  # type: ignore

from . import raw
from . import rich
from . import util


namespace_draw = 'urn:oasis:names:tc:opendocument:xmlns:drawing:1.0'
namespace_text = 'urn:oasis:names:tc:opendocument:xmlns:text:1.0'
attr_draw_name = namespace_draw, 'name'
attr_draw_page = namespace_draw, 'page'
tag_para = namespace_text, 'p'
attr_style = namespace_text, 'style-name'


def named_nodes(node: Any) -> Iterator[Any]:

    for subnode in node.findall('.//*[@{0}]'.format(util.format_qname(attr_draw_name))):
        if subnode.tag != util.format_qname(attr_draw_page):
            yield subnode.get(util.format_qname(attr_draw_name)), subnode


def get_names(b: bytes) -> set[str]:

    root = etree.XML(b)
    return set(name for name, _ in named_nodes(root))


def drop_style(node: Any) -> None:

    if util.format_qname(attr_style) in node.attrib:
        del node.attrib[util.format_qname(attr_style)]


Snippets: TypeAlias = dict[str, list[Any]]


def augment_snippets(b: bytes, snippets: Snippets) -> Snippets:

    result_snippets = dict(snippets)
    root = etree.XML(b)
    for name, node in named_nodes(root):
        if name in result_snippets:
            if result_snippets[name] is not None:
                raise KeyError('Duplicated name: {0}'.format(name))
            nodes = []
            for node in node.findall('.//{0}'.format(util.format_qname(tag_para))):
                for subnode in node.findall('.//*'):
                    drop_style(subnode)
                nodes.append(node)
            result_snippets[name] = nodes
    return result_snippets


def substitute_doc(doc: Any, snippets: Snippets) -> None:

    for name, node in named_nodes(doc):
        if name in snippets:
            nodes = snippets[name]
            for subnode in node:
                if node.tag == util.format_qname(tag_para):
                    node.remove(subnode)
            for subnode in nodes:
                node.append(subnode)


def inject(srcs: Iterable[str], template: str, dst: str) -> None:

    src_contents = dict((src, rich.content_of(src)) for src in srcs)
    template_content = rich.content_of(template)

    snippets: Snippets = \
      dict((name, []) for name in get_names(template_content))
    for src_content in src_contents.values():
        snippets = augment_snippets(src_content, snippets)
    snippets = dict((name, nodes) for name, nodes in snippets.items() if nodes is not None)

    shutil.copy(template, dst)
    dst_loc, _ = rich.directory_and_version_for_file(dst)
    rich.extract(dst, normalize = True, plain_basic = True)
    loc_content = path.join(dst_loc, raw.name_content)
    doc = etree.parse(loc_content)
    substitute_doc(doc, snippets)
    util.write_doc(loc_content, doc)
    rich.bundle(dst_loc, delete = True)
