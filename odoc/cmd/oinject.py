
'''Command-line interface for oinject.'''

# Author: 2023 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# This package is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; version 2.
#
# This package is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>
#
# On Debian systems, the complete text of the GNU General
# Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


__all__ = ['cmd']


import argparse
import argcomplete

from odoc import inject


parser = argparse.ArgumentParser(description = inject.__doc__.strip())

parser.add_argument('src', metavar = 'SRC', nargs = '+',
  help = 'sources to retrieve snippets from')
parser.add_argument('template', metavar = 'TEMPLATE',
  help = 'template to insert snippets into')
parser.add_argument('dst', metavar = 'DST',
  help = 'destination to create')


def cmd() -> None:

    argcomplete.autocomplete(parser)

    args = parser.parse_args()

    inject.inject(args.src, args.template, args.dst)
