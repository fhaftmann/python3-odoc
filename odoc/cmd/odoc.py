
'''Command-line interface for odoc.'''

# Author: 2023 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# This package is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; version 2.
#
# This package is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>
#
# On Debian systems, the complete text of the GNU General
# Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


__all__ = ['cmd']


from typing import Optional
from collections.abc import Callable
import argparse
import argcomplete
from datetime import datetime
from os import path

import odoc
from .. import version as version_reader
from .. import find
from .. import rich


class Timepoint:

    def __call__(self, txt: str) -> datetime:

        format = '%Y-%m-%dT%H:%M:%S.%f' if '.' in txt else '%Y-%m-%dT%H:%M:%S'
        return datetime.strptime(txt, format)

    def __repr__(self) -> str:

        return 'isoformat timepoint'


timepoint = Timepoint()


def extract(args: argparse.Namespace) -> None:

    if args.recursive:
        for arg_loc in args.locs:
            for src, _ in find.open_document_files(arg_loc):
                rich.extract(src, args.normalize, args.plain_basic, args.keep_destination,
                  args.timepoint, args.version_mangling, args.delete, args.target)
    else:
        for src in args.locs:
            rich.extract(src, args.normalize, args.plain_basic, args.keep_destination,
              args.timepoint, args.version_mangling, args.delete, args.target)


def bundle(args: argparse.Namespace) -> None:

    if args.recursive:
        for arg_loc in args.locs:
            for src, _ in find.open_document_directories(arg_loc):
                rich.bundle(src, args.prebuild, args.version_mangling, args.delete, args.target)
    else:
        for src in args.locs:
            rich.bundle(src, args.prebuild, args.version_mangling, args.delete, args.target, args.content, args.version)


def list_extracted(args: argparse.Namespace) -> None:

    if args.gitignore:
        def normalize(loc: str) -> str:
            return '/' + path.normpath(loc)
    else:
        def normalize(loc: str) -> str:
            return loc

    for arg_loc in args.locs:
        for src, _ in find.open_document_files(arg_loc):
            print(normalize(rich.directory_and_version_for_file(src, args.version_mangling)[0]))


def list_bundled(args: argparse.Namespace) -> None:

    if args.gitignore:
        def normalize(loc: str) -> str:
            return '/' + path.normpath(loc)
    else:
        def normalize(loc: str) -> str:
            return loc

    for arg_loc in args.locs:
        for src, _ in find.open_document_directories(arg_loc):
            print(normalize(rich.file_for_directory(src, args.version_mangling)))


def version(args: argparse.Namespace) -> None:

    loc = args.loc

    versions = set()
    for src, _ in find.open_document_directories(loc):
        current_version = version_reader.for_directory(src)
        if current_version is not None:
            versions.add(current_version)

    if len(versions) == 0:
        raise Exception('No versions found in {}'.format(loc))
    elif len(versions) > 1:
        raise Exception('Inconsistent versions found in {}: {}'.format(loc, ', '.join(versions)))
    else:
        print(versions.pop())


parser = argparse.ArgumentParser(description = odoc.__doc__.strip())
subparsers = parser.add_subparsers(required = True)

def add_parser(subcommand: Callable[[argparse.Namespace], None],
  help: str, epilog: Optional[str] = None) -> argparse.ArgumentParser:

    sub_parser = subparsers.add_parser(subcommand.__name__.replace('_', '-'),
      help = help, epilog = epilog)
    sub_parser.set_defaults(subcommand = subcommand)
    return sub_parser

sub_parser = add_parser(extract,
  'Extracts open document files.',
  'In non-recursive mode, PATHs refer to open document files; in recursive mode, PATHs refer to directories.')
sub_parser.add_argument('--recursive', '-r', action = 'store_true',
  help = 'operate recursively')
sub_parser.add_argument('--normalize', '-n', action = 'store_true',
  help = 'normalize (prettify) xml files')
sub_parser.add_argument('--plain-basic', '-p', action = 'store_true',
  help = 'write StarBasic modules as plain text')
sub_parser.add_argument('--keep-destination', action = 'store_true',
  help = 'keep already existing destination »as it is«')
sub_parser.add_argument('--timepoint', '-t', action = 'store', type = timepoint,
  metavar = 'YYYY-MM-DDTHH:MM:SS[.mmmmmm]',
  help = 'pin dynamic timestamps to specified timepoint (given in ISO format)')
sub_parser.add_argument('--version-mangling', action = 'store_true',
  help = 'consider user-defined version for stripping name suffix')
sub_parser.add_argument('--delete', '-d', action = 'store_true',
  help = 'delete original files')
sub_parser.add_argument('--target', action = 'store',
  help = 'alternative ground name for target')
sub_parser.add_argument('locs', metavar = 'PATH', nargs='+',
  help = 'paths to extract')

sub_parser = add_parser(bundle,
  'Bundles open document files from extracted directories.',
  'In non-recursive mode, PATHs refer to open document files; in recursive mode, PATHs refer to directories.\n'
  'Manifests are authoritative and checked against file system content for all file types except office extensions!')
sub_parser.add_argument('--recursive', '-r', action = 'store_true',
  help = 'operate recursively')
sub_parser.add_argument('--version', action = 'store',
  help = 'force version number for office extensions')
sub_parser.add_argument('--version-mangling', action = 'store_true',
  help = 'consider user-defined version for adding name suffix')
sub_parser.add_argument('--delete', '-d', action = 'store_true',
  help = 'delete original directories')
sub_parser.add_argument('--target', action = 'store',
  help = 'alternative ground name for target')
sub_parser.add_argument('--prebuild', '-p', action = 'store_true',
  help = 'execute pre-build executable if present')
sub_parser.add_argument('--content', '-c', metavar = 'PATH',
  help = 'alternative source of content (plain xml or open document)')
sub_parser.add_argument('locs', metavar = 'PATH', nargs='+',
  help = 'paths to bundle')

sub_parser = add_parser(list_extracted,
  'Prints directories that would emerge from extracting open document files.')
sub_parser.add_argument('--gitignore',  action = 'store_true',
  help = 'output in a format suitable for .gitignore files')
sub_parser.add_argument('--version-mangling', action = 'store_true',
  help = 'consider user-defined version for stripping name suffix')
sub_parser.add_argument('locs', metavar = 'PATH', nargs='+',
  help = 'paths to scan')

sub_parser = add_parser(list_bundled,
  'Prints files that would emerge from bundling open document directories.')
sub_parser.add_argument('--gitignore',  action = 'store_true',
  help = 'output in a format suitable for .gitignore files')
sub_parser.add_argument('--version-mangling', action = 'store_true',
  help = 'consider user-defined version for adding name suffix')
sub_parser.add_argument('locs', metavar = 'PATH', nargs='+',
  help = 'paths to scan')

sub_parser = add_parser(version,
  'Print consistent version of all extracted open documents in directory.  Failure if no consistent version present.')
sub_parser.add_argument('loc', metavar = 'PATH',
  help = 'path to scan')


def cmd() -> None:

    argcomplete.autocomplete(parser)

    args = parser.parse_args()

    exit_code = args.subcommand(args)
    if exit_code is not None and exit_code != 0:
        raise SystemExit(exit_code)
