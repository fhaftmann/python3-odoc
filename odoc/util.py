
'''Misc utilities.'''

# Author: 2023 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# This package is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; version 2.
#
# This package is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>
#
# On Debian systems, the complete text of the GNU General
# Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


__all__ = ['utf8', 'info', 'null', 'is_prefix_of', 'common_prefix_suffixes',
  'explicit_basename', 'explode_path', 'implode_path', 'list_files_tree',
  'format_qname', 'read_doc_permissive', 'write_doc', 'indent', 'indent_file']


from typing import Optional, Any, TypeVar
from collections.abc import Iterator, Sized, Callable, Sequence, Iterable
from io import BufferedReader, BufferedWriter
import os
from os import path
import sys

from lxml import etree  # type: ignore


A = TypeVar('A')


utf8 = 'utf-8'


# diagnostic

def info(txt: str) -> None:

    print(txt, file = sys.stderr)


# sequences

def null(xs: Sized | Iterable[A]) -> bool:

    if isinstance(xs, Sized):
        return len(xs) == 0
    else:
        for _ in xs:
            return False
        return True


def is_prefix_of(xs: Sequence[A], ys: Sequence[A]) -> bool:

    return ys[:len(xs)] == xs


def common_prefix_suffixes(xs: Sequence[A], ys: Sequence[A]) -> tuple[Sequence[A], Sequence[A], Sequence[A]]:

    zs = []
    n = 0
    n_max = min(len(xs), len(ys))
    while n < n_max and xs[n] == ys[n]:
        zs.append(xs[n])
        n += 1
    return zs, xs[n:], ys[n:]


# filenames

def explicit_basename(loc: str, suffix: str = '') -> str:

    raw_basename = path.basename(loc)
    if raw_basename == os.sep:
        raise Exception('Bad directory: {}'.format(loc))
    elif raw_basename in [os.curdir, os.pardir, '']:
        proper_basename = path.basename(path.abspath(loc))
        return path.join(loc, os.pardir, proper_basename + suffix)
    else:
        return loc + suffix


def explode_path(loc: str, split: Callable[[str], tuple[str, str]] = path.split) -> Sequence[str]:

    if loc == '':
        return ()

    parts = []
    loc_old = None
    while loc != loc_old:
        loc_old = loc
        loc, base = split(loc_old)
        if base != '':
            parts.append(base)
    if loc != '' and all(c == os.sep for c in loc):  ## consider absolute paths
        parts.append(os.sep)

    return tuple(reversed(parts))


def implode_path(parts: Sequence[str], join: Callable[[str, str], str] = path.join) -> str:

    return join(*parts) if parts else ''


def combine(loc1: str, loc2: str) -> tuple[str, str, str]:

    parts1 = explode_path(loc1)
    parts2 = explode_path(loc2)

    prefix, suffix1, suffix2 = common_prefix_suffixes(parts1, parts2)

    return implode_path(prefix), implode_path(suffix1), implode_path(suffix2)


def list_files_tree(loc: str) -> Iterator[str]:

    for dir_loc, _, file_names in os.walk(loc):
        _, _, prefix = combine(loc, dir_loc)
        for file_name in file_names:
            yield path.join(prefix, file_name)


# xml

def format_qname(qname: tuple[str, str]) -> str:

    return '{' + qname[0] + '}' + qname[1]


def read_doc_permissive(src: str) -> Optional[Any]:

    try:
        doc = etree.parse(src)
    except etree.XMLSyntaxError:
        return None
    return doc


def write_doc(dst: str | BufferedWriter, doc: Any) -> None:

    doc.write(dst, encoding = doc.docinfo.encoding,
      standalone = doc.docinfo.standalone, xml_declaration = True)


def indent_elem(elem: Any, mixed_content: Iterable[tuple[str, str]], indent: int = 2) -> None:

    tags_content = [format_qname(qname) for qname in mixed_content]

    def indent_(level: int, elem: Any) -> None:

        if len(elem) > 0 and elem.tag not in tags_content:
            sub_level = level + 1
            sub_indent = '\n' + (' ' * indent * sub_level)
            elem.text = sub_indent
            for sub_elem in elem[:-1]:
                sub_elem.tail = sub_indent
                indent_(sub_level, sub_elem)
            elem[-1].tail = '\n' + (' ' * indent * level)
            indent_(sub_level, elem[-1])

    indent_(0, elem)


def indent(reader: BufferedReader, writer: BufferedWriter, mixed_content: Iterable[tuple[str, str]],
  indent: int = 2) -> None:

    doc = etree.parse(reader)
    indent_elem(doc.getroot(), mixed_content, indent)
    write_doc(writer, doc)


def indent_file(loc: str, mixed_content: Iterable[tuple[str, str]], indent: int = 2) -> None:

    doc = etree.parse(loc)
    indent_elem(doc.getroot(), mixed_content, indent)
    with open(loc, 'wb') as writer:
        write_doc(writer, doc)
