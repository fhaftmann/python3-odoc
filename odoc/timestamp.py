
'''Explicit pinning of dynamic timestamps in open document xml files.'''

# Author: 2023 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# This package is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; version 2.
#
# This package is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>
#
# On Debian systems, the complete text of the GNU General
# Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


__all__ = ['timestamp']


from typing import Any, ParamSpec, Concatenate
from collections.abc import Callable
from os import path
from datetime import datetime

from lxml import etree  # type: ignore

from . import util
from . import raw


P = ParamSpec('P')


namespace_office = 'urn:oasis:names:tc:opendocument:xmlns:office:1.0'
namespace_table = 'urn:oasis:names:tc:opendocument:xmlns:table:1.0'
namespace_text = 'urn:oasis:names:tc:opendocument:xmlns:text:1.0'
namespace_meta = 'urn:oasis:names:tc:opendocument:xmlns:meta:1.0'

cell_tag = util.format_qname((namespace_table, 'table-cell'))
cell_formula_attr = util.format_qname((namespace_table, 'formula'))
cell_datetime_attr = util.format_qname((namespace_office, 'date-value'))

date_tag = util.format_qname((namespace_text, 'date'))
date_attr = util.format_qname((namespace_text, 'date-value'))
time_tag = util.format_qname((namespace_text, 'time'))
time_attr = util.format_qname((namespace_text, 'time-value'))

creation_tag = util.format_qname((namespace_meta, 'creation-date'))

formula_now = 'of:=NOW()'
formula_today = 'of:=TODAY()'


def reset(attrib: dict[str, str], tag: str, value: str) -> None:

    if tag in attrib:
        attrib[tag] = value


def timestamp_content(elem: Any, datetime_text: str, date_text: str) -> None:

    if elem.tag == cell_tag and elem.attrib.get(cell_formula_attr) == formula_now:
        for sub_elem in elem:
            elem.remove(sub_elem)
        elem.text = None
        reset(elem.attrib, cell_datetime_attr, datetime_text)
    elif elem.tag == cell_tag and elem.attrib.get(cell_formula_attr) == formula_today:
        for sub_elem in elem:
            elem.remove(sub_elem)
        elem.text = None
        reset(elem.attrib, cell_datetime_attr, date_text)
    else:
        for sub_elem in elem:
            timestamp_content(sub_elem, datetime_text, date_text)


def timestamp_styles(elem: Any, date_text: str, time_text: str) -> None:

    if elem.tag == date_tag:
        reset(elem.attrib, date_attr, date_text)
    elif elem.tag == time_tag:
        reset(elem.attrib, time_attr, time_text)

    for sub_elem in elem:
        timestamp_styles(sub_elem, date_text, time_text)


def timestamp_meta(elem: Any, datetime_text: str) -> None:

    if elem.tag == creation_tag:
        elem.text = datetime_text

    for sub_elem in elem:
        timestamp_meta(sub_elem, datetime_text)


def transform(loc: str,
  f: Callable[Concatenate[Any, P], None], *args: P.args, **kwargs: P.kwargs) -> None:

    if path.exists(loc):
        doc = etree.parse(loc)
        f(doc.getroot(), *args, **kwargs)
        with open(loc, 'wb') as writer:
            util.write_doc(writer, doc)


def timestamp(loc_dir: str, timepoint: datetime) -> None:

    assert isinstance(timepoint, datetime)

    date_text = str(timepoint.date().isoformat())
    time_text = '0000-00-00T' + str(timepoint.time().isoformat())
    datetime_text = str(timepoint.isoformat())

    transform(path.join(loc_dir, raw.name_content),
      timestamp_content, datetime_text, date_text)
    transform(path.join(loc_dir, raw.name_styles),
      timestamp_styles, date_text, time_text)
    transform(path.join(loc_dir, raw.name_meta),
      timestamp_meta, datetime_text)
