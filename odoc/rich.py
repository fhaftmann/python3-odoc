
'''Rich extracting and bundling of open document files.'''

# Author: 2023 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# This package is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; version 2.
#
# This package is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>
#
# On Debian systems, the complete text of the GNU General
# Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


__all__ = ['directory_and_version_for_file', 'extract', 'file_for_directory', 'bundle']


from typing import Optional
from datetime import datetime
from os import path
import os
import subprocess

from . import util
from . import ziputil
from . import mime
from . import normalization
from . import timestamp
from . import star_basic
from . import version
from . import raw
from . import extension


# extracting

def split_version(loc: str) -> tuple[str, Optional[str]]:

    loc_dir, name = path.split(loc)
    base, _ = path.splitext(name)
    fragments = base.split('_')
    core, version_candidate = '_'.join(fragments[:-1]), fragments[-1]
    if all(c in '0123456789.' for c in version_candidate):
        return path.join(loc_dir, core), version_candidate
    else:
        util.info('WARNING – no version suffix for {}'.format(loc))
        return path.join(loc_dir, base), None


def directory_and_version_for_file(loc: str, version_mangling: bool = False,
  target: Optional[str] = None) -> tuple[str, Optional[str]]:

    if not path.isfile(loc):
        raise Exception('Bad file: {}'.format(loc))

    loc_dir = path.splitext(loc)[0]
    if loc_dir == loc:
        raise Exception('Bad file name: {}'.format(loc))

    return (target, None) if target is not None \
      else split_version(loc) if version_mangling \
      else (loc_dir, None)


def extract(src: str, normalize: bool = False, plain_basic: bool = False, keep_destination: bool = False,
  timepoint: Optional[datetime] = None, version_mangling: bool = False, delete: bool = False,
  target: Optional[str] = None) -> None:

    mime_type = mime.assert_odoc(mime.guess_type(src))
    is_extension = mime.is_extension(mime_type)
    version_mangling = not is_extension and version_mangling

    dst, some_version_from_file = directory_and_version_for_file(src, version_mangling, target)
    version_from_file = None
    if version_mangling:
        if some_version_from_file is None:
            raise Exception('No version found')
        version_from_file = some_version_from_file
    raw.extract(src, dst, keep_destination, delete)

    if timepoint is not None:
        timestamp.timestamp(dst, timepoint)
    if plain_basic:
        star_basic.planify(dst, is_extension, delete)
    if version_from_file is not None:
        version.set_for_directory(dst, version_from_file)
    if normalize and not is_extension:
        normalization.indent(dst)


# bundling

def content_of(loc: str) -> bytes:

    mime_type = mime.guess_type(loc)
    if mime_type in mime.extension_for:
        content = ziputil.get_file(loc, raw.name_content)
        return content
    else:
        with open(loc, 'rb') as reader:
            content = reader.read()
        return content


def file_for_directory(loc: str, version_mangling: bool = False, target: Optional[str] = None) -> str:

    if not path.isdir(loc):
        raise Exception('Bad directory: {}'.format(loc))

    mime_type = mime.assert_odoc(mime.guess_type_of_directory(loc))

    suffix = '.' + mime.extension_for[mime_type]
    if version_mangling and not mime.is_extension(mime_type):
        current_version = version.for_directory(loc)
        if current_version is None:
            raise Exception('No version found for {}'.format(loc))
        suffix = '_' + current_version + suffix
    return util.explicit_basename(loc if target is None else target, suffix)


def bundle(src: str, prebuild: bool = False, version_mangling: bool = False, delete: bool = False,
  target: Optional[str] = None, alternative_content: Optional[str] = None,
  alternative_version: Optional[str] = None) -> None:

    prebuild_exec = util.explicit_basename(src, '.prebuild')
    if prebuild and path.exists(prebuild_exec):
        util.info('Prebuilding using {}…'.format(prebuild_exec))
        subprocess.run([path.abspath(prebuild_exec)], cwd = path.dirname(prebuild_exec), check = True)

    is_extension = mime.is_extension(mime.assert_odoc(mime.guess_type_of_directory(src)))

    description = None
    if alternative_version is not None and is_extension:
        loc_description = path.join(src, extension.name_description)
        description = extension.force_version_for_description(loc_description, alternative_version)

    content = content_of(alternative_content) if alternative_content is not None else None

    dst = file_for_directory(src, version_mangling, target)

    ignored_files, generated_files = star_basic.xmlify(src, is_extension, delete)

    raw.bundle(src, dst, delete, ignored_files, alternative_content = content, alternative_description = description)

    if not delete:
        for loc_file in generated_files:
            os.remove(loc_file)
