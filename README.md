
odoc
====

This library provides abstraction and manipulation devices for OASIS Open
Document files, plus the unofficial cousins Open Document Base files and
LibreOffice / OpenOffice extensions.

Particular focus is set on (un)bundling zipped files to (from) bare files
suitable for versioning, with normalized xml, plain text basic macro
text, implicit version numbering.
